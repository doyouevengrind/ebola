﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ebola
{
    public partial class Form2 : Form
    {
        private List<Form1> forms = new List<Form1>();
        private int delay = 200;
        private Timer MyTimer;

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            forms.Add(new Form1());
            forms.Last<Form1>().Show();
            MyTimer = new Timer();
            MyTimer.Interval = delay;
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            MyTimer.Start();
            //delay /= 2;
            //if (delay < 100) delay = 200;
        }

        private void MyTimer_Tick(object sender, EventArgs args)
        {
            forms.Add(new Form1());
            forms.Last<Form1>().Show();
            //MyTimer.Interval = delay;
            //delay /= 2;
            //if (delay < 100) delay = 200;
        }
    }
}
