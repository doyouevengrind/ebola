﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace ebola
{
    public partial class Form1 : Form
    {
        private static string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        private static Image removed = Image.FromFile("removed.png");

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Random random = new Random();
            string str;
            WebRequest request;
            Stream stream;
            
            do
            {
                str = "";
                for (int i = 0; i < 5; i++) str += letters.Substring(random.Next(62), 1);
                request = WebRequest.Create("http://i.imgur.com/" + str + ".jpg");
                stream = request.GetResponse().GetResponseStream();
                pictureBox1.Image = Bitmap.FromStream(stream);
            } while (pictureBox1.Image.Size == removed.Size);
            Size newSize = pictureBox1.Image.Size;
            if (newSize.Width > 1000)
            {
                newSize.Height = (int)(newSize.Height * (1000f / (float)newSize.Width));
                newSize.Width = 1000;
            }
            if (newSize.Height > 800)
            {
                newSize.Width =  (int)(newSize.Width * (800f / (float)newSize.Height));
                newSize.Height = 800;
            }
            ClientSize = newSize;
            Left = random.Next(Screen.FromControl(this).Bounds.Width - Size.Width);
            Top = random.Next(Screen.FromControl(this).Bounds.Height - Size.Height);
        }
    }
}
